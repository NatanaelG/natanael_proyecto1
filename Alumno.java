import java.util.*;
public class Alumno extends Persona{
	private boolean inscrito;
	private ArrayList<String>materiasAlumno;

	public void agegarMaterias(List<String> materias, String mat){
    materias = new ArrayList<String>();

	    materias.add("Espa�ol");
	    materias.add("Matematicas");
	    materias.add("Historia");
	}
	
	public Alumno(int clave, String nombres, String apellidos, boolean inscrito, ArrayList<String>materiasAlumno){
	super(clave, nombres, apellidos);
	this.inscrito=inscrito;
	this.materiasAlumno=materiasAlumno;
	}

	public void setinscrito (boolean inscrito){
		this.inscrito=inscrito;
	}

	public boolean getinscrito (){
		return inscrito;
	}

	public void setmateriasAlumnos (ArrayList<String> materias){
		this.materiasAlumno= materiasAlumno;
	}

	public ArrayList<String> getmateriasAlumnos(){
		return materiasAlumno;
	}

	public String toString(){
		String alum = "Inscrito: " + getinscrito() + "\nPersona Materias" + getmateriasAlumnos();
		return alum;
	}

}