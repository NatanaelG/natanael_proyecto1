public class Empleado extends PersonaContable{

	private String puesto;
	private double sueldobase;
	private double bono;

	public Empleado(int clave, String nombres, String apellidos, String cuenta_contable, int clabe_interbancaria, String banco, short cuenta_bancaria, String puesto, double sueldobase, double bono){
	super(clave, nombres, apellidos, cuenta_contable, clabe_interbancaria, banco, cuenta_bancaria);
	this.puesto=puesto;
	this.sueldobase=sueldobase;
	this.bono=bono;
	}


	public void setpuesto (String puesto){
		this.puesto=puesto;
	}

	public String getpuesto (){
		return puesto;
	}

	public void setsueldobase (double sueldobase){
		this.sueldobase=sueldobase;
	}

	public double getsueldobase (){
		return sueldobase;
	}

	public void setbono (double bono){
		this.bono=bono;
	}

	public double getbono (){
		return bono;
	}

	public String toString(){
		String emp = "Puesto: " + getpuesto() + "\nSueldo Base: " + getsueldobase() + "\nBono: " + getbono();
		return emp;
	}
}
