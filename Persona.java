public class Persona{

	private int clave;
	private String nombres;
	private String apellidos;

	Persona(int clave, String nombres, String apellidos){
		this.clave=clave;
		this.nombres=nombres;
		this.apellidos=apellidos;
	}

		public void setclave (int clave){
		this.clave=clave;
	}

	public int getclave(){
		return clave;
	}	

	public void setnombres (String nombres){
		this.nombres=nombres;
	}

	public String getnombres(){
		return nombres;
	}

	public void setapellidos (String apellidos){
		this.apellidos=apellidos;
	}

	public String getapellidos(){
		return apellidos;
	}

	public String toString(){
		String per = "Clave: " + getclave() + "\nNombres: " + getnombres() + "Apellidos: " + getapellidos() + "";
		return per;
	} 

}