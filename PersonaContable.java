public class PersonaContable extends Persona{

	private String cuenta_contable;
	private int clabe_interbancaria;
	private String banco;
	private short cuenta_bancaria;

	public PersonaContable(int clave, String nombres, String apellidos, String cuenta_contable, int clabe_interbancaria, String banco, short cuenta_bancaria){
	super(clave, nombres, apellidos);
	this.cuenta_contable = cuenta_contable;
	this.clabe_interbancaria = clabe_interbancaria;
	this.banco = banco;
	this.cuenta_bancaria = cuenta_bancaria;
	}

	public void setcuenta_contable(String cuenta_contable){
		this.cuenta_contable = cuenta_contable;
	}

	public String getcuenta_contable(){
		return cuenta_contable;
	}

	public void setclabe_interbancaria(int clabe_interbancaria){
		this.clabe_interbancaria=clabe_interbancaria;
	}

	public int getclabe_interbancaria(){
		return clabe_interbancaria;
	}

	public void setbanco (String banco){
		this.banco=banco;
	}

	public String getbanco(){
		return banco;
	}

	public void setcuenta_bancaria (short cuenta_bancaria){
		this.cuenta_bancaria=cuenta_bancaria;
	}

	public short getcuenta_bancaria(){
		return cuenta_bancaria;
	}

	public String toString(){
		String perCon = "Cuenta Contable: " + getcuenta_contable() + "\nClabe Interbancaria"  + getclabe_interbancaria() + "\nBanco" + getbanco() + "\nCuenta bancaria" + getcuenta_bancaria();
		return perCon;
	}


}