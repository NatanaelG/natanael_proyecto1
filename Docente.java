import java.util.*;
public class Docente extends Empleado{

	private String tipoDocente;
	private String gradoAcademico;
	private List<String> materiasDocente;
    
	/*public void agegarMaterias(List<String> materias, String mat){
    materias = new ArrayList<String>();

	    materias.add("Espa�ol");
	    materias.add("Matematicas");
	    materias.add("Historia");
	}*/

	public Docente (int clave, String nombres, String apellidos, String cuenta_contable, int clabe_interbancaria, String banco, short cuenta_bancaria, String puesto, double sueldobase, double bono, String tipoDocente, String gradoAcademico, List<String>materiasDocente){
	super(clave, nombres, apellidos, cuenta_contable, clabe_interbancaria, banco,cuenta_bancaria, puesto, sueldobase, bono);
	this.tipoDocente=tipoDocente;
	this.gradoAcademico=gradoAcademico;	
	this.materiasDocente=materiasDocente;
	}

	public void settipoDocente (String tipoDocente){
		this.tipoDocente=tipoDocente;
	}

	public String gettipoDocente (){
		return tipoDocente;
	}

	public void setgradoAcademico(String gradoAcademico){
		this.gradoAcademico=gradoAcademico;
	}

	public String getgradoAcademico(){
		return gradoAcademico;
	}

	public void setmateriasDocentes (List<String> materias){
		this.materiasDocente= materiasDocente;
	}

	public List<String> getmateriasDocentes(){
		return materiasDocente;
	}
		
	public String toString(){
		String doc = "Tipo Docente: " + gettipoDocente() + "\nGrado Academico"  +  getgradoAcademico() + "\nMaterias: " + getmateriasDocentes();
		return doc;
	}
}

