import java.util.*; 
public class Administrativo extends Empleado{

	private String tipoadministrativo;
	private Calendar antiguedad_base;

	public Administrativo (int clave, String nombres, String apellidos, String cuenta_contable, int clabe_interbancaria, String banco, int cuenta_bancaria, String puesto, double sueldobase, double bono, String tipoadministrativo, Calendar antiguedad_base){
		super(clave, nombres, apellidos, cuenta_contable, clabe_interbancaria, banco, cuenta_bancaria, puesto, sueldobase, bono);
		this.tipoadministrativo = tipoadministrativo;
		this.antiguedad_base=antiguedad_base;
	}

	public void settipoadministrativo (String tipoadministrativo){
		this.tipoadministrativo=tipoadministrativo;
	}

	public String gettipoadministrativo (){
		return tipoadministrativo;
	}

	public void setantiguedad_base (Calendar antiguedad_base){
		this.antiguedad_base=antiguedad_base;
	}

	public Calendar getantiguedad_base (){
		return antiguedad_base;
	}

	public String toString (){
		String admin  ="Administrativo: " + gettipoadministrativo() + "\nAntiguedad: " + getantiguedad_base();
		return admin;
	}


	}
